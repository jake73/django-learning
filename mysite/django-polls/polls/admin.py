from django.contrib import admin
from .models import Question, Choice
# Register your models here.

class ChoiceInline(admin.TabularInline): #tabular makes it look like an excel type table
    model = Choice
    extra = 3 #amount of choice fields shown in related model's form

class QuestionAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Question Details', {'fields': ['question_text']}),
        ('Date Information', {'fields': ['pub_date'], 'classes': ['collapse']})
    ]

    list_display = ('question_text', 'pub_date', 'was_published_recently') # you can call model methods!
    list_filter = ['pub_date']

    search_fields = ['question_text']

    inlines = [ChoiceInline]

admin.site.register(Question, QuestionAdmin)
